const num = 2
const getCube = num**3
console.log(`The cube of ${num} is ${getCube}`)

const address = ["258", "Washigton Ave NW", "California", "90011"]
const [houseNumber, streetName, state, zipCode] = address

console.log(`I live at ${houseNumber} ${streetName} ${state} ${zipCode}`)

const animal = {
	name: 'Lolong',
	animalType: 'saltwater crocodile',
	weight: '1075 kgs',
	size: '20 ft 3 in'
};

const{ name, animalType, weight, size } = animal;
console.log(`${name} was at ${animalType}. He weighed at ${weight} with a measurement of ${size}`)

const numbers = [1,2,3,4,5]
const [num1, num2, num3, num4, num5] = numbers
numbers.forEach((number) => console.log(`${number}`))

const sum = (a,b,c,d,e) => a + b + c + d + e 

let reduceNumber = sum(1,2,3,4,5);
console.log(reduceNumber)

class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const dog1 = new Dog();
dog1.name = 'Frankie';
dog1.age = 5;
dog1.breed = 'Miniature Dachsund'

console.log(dog1)



